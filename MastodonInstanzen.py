#!/usr/bin/env python
# https://www.digitalocean.com/community/tutorials/how-to-get-started-with-the-requests-library-in-python-de
import json
import requests
import math
from collections import OrderedDict

instances=[line.rstrip('\n') for line in open("instances-urls.txt")]

TEMPLATE="""
| Instanz | Admin | LTL | Block | Offen | Einladung | Version | User | UserM | Posts | Zeichen |
|-|-|-|-|-|-|-|-:|-:|-:|-:|
"""

output = TEMPLATE

# Get the data from every instance in the instances-urls.txt file
result = {}
for instance in instances:
  try:
    # Einlesen der API Daten
    req_api        = requests.get(instance + "/api/v1/instance")
    if 'application/json' in req_api.headers.get('Content-Type'):
      data_api     = json.loads(req_api.text)
      bool_reg     = data_api.get("registrations", False);
      bool_approv  = data_api.get("approval_required", False);
      str_version  = data_api["version"]
      num_statuses = data_api["stats"]["status_count"]
      num_chars    = data_api.get("max_toot_chars",500)
      str_mail     = data_api.get("email", "-").replace(" [a] ", "@")
      str_admin    = data_api.get("contact_account", {}).get("url", "https://fehlt")
      str_name     = data_api.get("contact_account", {}).get("username", "")
      str_block    = instance + "/about/more#unavailable-content"
      # str_lang     = data_api["languages"]["0"]
      str_lang     = data_api.get("languages")
      
      # Ab hier werden den Daten aus der Nodeinfo eingelesen
    req_nod          = requests.get(instance + "/nodeinfo/2.0")
    if 'application/json' in req_nod.headers.get('Content-Type'):
        data_nod     = json.loads(req_nod.text)
        num_users    = data_nod["usage"]["users"]["total"]
        num_activm   = data_nod["usage"]["users"]["activeMonth"]
        num_activhy  = data_nod["usage"]["users"]["activeHalfyear"]
        num_q        = round(data_nod["usage"]["users"]["activeMonth"] / data_nod["usage"]["users"]["total"] , 2)

    result[instance]={
    "bool_reg":bool_reg,
    "bool_approv":bool_approv,
    "str_version3":str_version[:3],
    "str_version":str_version,
    "num_statuses":num_statuses,
    "num_chars":num_chars,
    "str_mail":str_mail,
    "str_admin":str_admin,
    "str_name":str_name,
    "str_block":str_block,
    "str_lang":str_lang,
    "num_users":num_users,    
    "num_activm":num_activm,
    "num_activhy":num_activhy,
    "num_q":num_q}

  except requests.exceptions.RequestException as e:
    print("Error fetching data from: {instance}".format(instance=instance))

# Sort the result by str_version und num_users
sorted_result = OrderedDict(
    sorted(result.items(),
    key=lambda instance: (
        instance[1]["bool_reg"],
        not instance[1]["bool_approv"],
        instance[1]["str_version"],
        #(instance[1]["num_statuses"] / instance[1]["num_users"]) * math.sqrt(instance[1]["num_users"])
        instance[1]["num_activm"] if instance[1]["num_activm"] < 2000 else -instance[1]["num_activm"]
        #instance[1]["num_activm"]
    ), reverse=True))

# Output it as a table
for instance,value in sorted_result.items():
  output += "| [{instance2} {str_lang}]({instance}) | ([{str_name}]({str_admin})) | [🕙]({ltl}) | [🔇]({str_block}) | {bool_reg} | {bool_approv} | {str_version} | {num_users} | {num_activm} ({num_q}) | {num_statuses} | {num_chars} | \n".format(
            instance     = instance,
            instance2    = instance.replace("https://", ""),
            ltl          = 'http://www.unmung.com/mastoview?url=' + instance.replace("https://", "") + '&view=local',
            bool_reg     = "✅" if value["bool_reg"] else "⛔",
            bool_approv  = "⛔" if value["bool_approv"] else "✅",
            str_version  = value["str_version"],
            num_users    = value["num_users"],
            num_activm   = value["num_activm"],
            num_statuses = value["num_statuses"],
            num_chars    = value["num_chars"],
            str_mail     = value["str_mail"],
            str_admin    = value["str_admin"],
            str_name     = value["str_name"],
            str_block    = value["str_block"],
            str_lang     = "🇩🇪" if value["str_lang"] == ['de'] else "",
            #str_lang     = value["str_lang"], ['en'] ['de']
            num_q        = value["num_q"]
)

print(output)